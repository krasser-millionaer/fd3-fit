#!/usr/bin/env python3

import sys, os, io
from contextlib import redirect_stdout

from typing import Dict, List, Tuple, Union
from dataclasses import dataclass
from configparser import ConfigParser

from math import exp

from numpy import linspace, power, add, multiply, ndarray, array

from scipy.interpolate import splrep, splev
from scipy.linalg import eigh_tridiagonal
from scipy.optimize import least_squares



class Logger:
    """
    Enables output to terminal and to text file.
    """
    def __init__(self, filein):
        self.log = open('{0}.log'.format(filein), 'w')

    def write(self, message, end='\n'):
        print(message, end=end)
        self.log.write(message + end)

    def finalize(self):
        self.log.close()

@dataclass
class Level:
    energy: float
    rot_const: float
    #r_grid: ndarray
    r_grid: List[float]
    wavef_grid: List[float]


def read_input(
    filename: str
) -> Tuple[Dict, Tuple[float, ...], Tuple[float, ...], Dict[int, Dict[int, float]]]:
    '''
    Opens input and parses it
    Section [Paramters] expected to contain keyword + parameter value
    Section [PEC] expected to contain pointwise PEC
    '''
    # Fixed and fitting parameters
    input_parser = ConfigParser(delimiters=(' ', '\t'))

    input_parser.read(filename)

    params: Dict = {}
    
    for keyword, value in input_parser['Parameters'].items():
        if keyword in ('q', 'p', 'refit'):
            params[keyword] = int(value)
        elif keyword in ('mass1', 'mass2', 'rmin',  'rmax', 're', 'de', 'rref'):
            params[keyword] = float(value)
        elif keyword in ('expdata'):
            params[keyword] = str(value)
        elif keyword in ('beta', 'cn'):
            params[keyword] = list(map(float, value.split()))
        elif keyword in ('cnpow'):
            params[keyword] = list(map(int, value.split()))
        else:
            raise KeyError(f'Unknown keyword: {keyword}')
    
    # Required paramters check
    if 'mass1' not in params or 'mass2' not in params:
        raise RuntimeError('One or two nuclear masses are missed')
    if 'expdata' not in params:
        raise RuntimeError('File with experimental data is not specified')
    if 'q' not in params:
        raise RuntimeError('Parameter "q" is not specified')
    if 'rref' not in params:
        raise RuntimeError('Parameter "Rref" is not specified')

    # PEC storage and sort by R
    r_inp: Tuple[float, ...] = tuple(map(float, input_parser['PEC'].keys()))
    u_inp: Tuple[float, ...] = tuple(map(float, input_parser['PEC'].values()))
    r_inp, u_inp = zip(*sorted(zip(r_inp, u_inp), key = lambda x: x[0]))
    
    # Default parameters
    if 'rmin' not in params:
        params['rmin'] = r_inp[0]
    if 'rmax' not in params:
        params['rmax'] = r_inp[-1]
    if 'refit' not in params:
        params['refit'] = 0

    # Exp. data parse
    input_parser = ConfigParser(delimiters=(' ', '\t'))
    
    input_parser.read(params['expdata'])
    
    expdata = {}
    for j in input_parser.sections():
        tmp = {}
        for v, e in input_parser[j].items():
            tmp[int(v)] = float(e)
        expdata[int(j)] = tmp

    return params, r_inp, u_inp, expdata

def emo(
    r, 
    d_e, 
    r_e, 
    r_ref, 
    q, 
    beta
) -> float:
    '''   
    Calculates EMO with given parameters set
    '''
    y = (r**q - r_ref**q) / (r**q + r_ref**q)
    
    beta_pol = beta[0]
    for n in range(1, len(beta)):
        beta_pol += beta[n] * y**n
    
    return d_e * (1 - exp(-beta_pol * (r - r_e)))**2

def res_emo(
guess, 
r_inp, 
u_inp,  
r_ref, 
q
) -> List[float]:
    '''
    Generates residual for "scipy.optimize.least_squares" routine for PEC
    '''
    # Sends fitting parameters to EMO function
    d_e = guess[0]
    r_e = guess[1]
    beta = guess[2:]
    u_fit = map(lambda x : emo(x, d_e, r_e, r_ref, q, beta), r_inp) 
    
    # Type transfrom and residual calculation
    u_inp = array(u_inp)
    u_fit = array(list(u_fit))
    res = u_fit - u_inp
    
    return res

def emo_init_fit(
    d_e,
    r_e,
    q,
    r_ref,
    beta,
    r_inp,
    u_inp
) -> Tuple[float, float, List[float]]:
    '''
    Performs least square EMO fit of given PEC 
    '''
    guess = [d_e, r_e]
    guess.extend(beta) 
    add_args = (r_inp, u_inp, r_ref, q)
    
    res_1 = least_squares(res_emo, guess, args = add_args)
    
    return res_1.x[0], res_1.x[1], res_1.x[2:]

def vr_solver(
    params: Dict[str, Union[int, float]],
    expdata,
    d_e, 
    r_e, 
    beta
) -> Dict[int, Dict[int, Level]]:
    '''
    Calculates vib.-rot. energies and wavefunctions for given set of parameters and PEC
    '''
    levels: Dict[int, Dict[int, Level]] = {}

    # Reduced mass
    mu = params['mass1'] * params['mass2'] / (params['mass1'] + params['mass2'])
    
    # h^2 / (2*mu) [cm-1 * A^2]
    scale = au_to_Da * au_to_cm * pow(a0_to_A, 2) / (2 * mu)

    # Generates grid for R with step l_step
    l_step = (params['rmax'] - params['rmin'])/(ngrid - 1)
    r_grid = linspace(params['rmin'], params['rmax'], ngrid)

    # Calls EMO func to find PEC values
    u_grid = array(list(map(lambda x: emo(x, d_e, r_e, params['rref'], params['q'], beta), r_grid)))

    # Loop over J to calculate level energies
    for j in expdata.keys(): 
        # Pointwise 1/R^2
        oneByR2 = power(r_grid, -2)

        # Diagonal elements (ngrid)
        diagonal = add(u_grid / scale, j * (j + 1) * oneByR2) + 2 / power(l_step, 2)

        # Off-diagonal elements (ngrid-1)
        off_diag = [-power(l_step, -2)] * (ngrid - 1)

        # Call SciPy routine to calculate eigenvalues and eigenvectors
        results = eigh_tridiagonal(
            diagonal,
            off_diag,
            select='i',
            select_range=(sorted(expdata[j].keys())[0], sorted(expdata[j].keys())[-1])
            )

        #Storage for levels found
        levels[j] = {}
        for v, en in enumerate(results[0]):
            lev = Level(
                en * scale,
                scale * sum(multiply(power(results[1][:, v], 2), oneByR2)),
                list(r_grid),
                results[1][:, v]
                )
            levels[j][v] = lev

    return levels

def res_exp(
    guess, 
    params: Dict[str, Union[int, float]],
    expdata
) -> List[float]:
    '''
    Generates residual for "scipy.optimize.least_squares" routine for vib.-rot. energies
    '''
    # Sends fitting parameters of EMO to solver function
    d_e = guess[0]
    r_e = guess[1]
    beta = guess[2:]
    levels = vr_solver(params, expdata, d_e, r_e, beta)
    
    # Residual calculation
    res =[]
    for j, _ in levels.items():
        for v, lev in levels[j].items():
            res.append(lev.energy - expdata[j][v])
    
    return res

def exp_fit(
    params, 
    expdata, 
    d_e, 
    r_e, 
    beta
) -> Tuple[float, float, List[float]]:
    '''
    Performs least square EMO fit for given for vib.-rot. energies 
    '''
    guess = [d_e, r_e]
    guess.extend(beta)
    add_args = (params, expdata)
    
    res_1 = least_squares(res_exp, guess, verbose = 2, args = add_args)
    
    return res_1.x[0], res_1.x[1], res_1.x[2:]

def main() -> None:
    '''
    The program to perform least square optimization of potential energy curve (PEC)
    of diatomic molecule ground X1Sigma state to fit given set of experimental levels.
    The finite-difference scheme for solving the radial Schrodinger equation
    is used to find the vibrational-rotational levels.
    The given PEC is approximated with Extended Morse Oscillator (EMO) function.
    The EMO parameters are optimized to reproduce experimental levels.
    See ``README.md for details''
    '''
    # 1st cmd arg -> name of input file, exit if not given
    if len(sys.argv) > 1:
        params, r_inp, u_inp, expdata = read_input(sys.argv[1])
    else:
        raise RuntimeError('Usage: fd3.py <input file>')

    logfile = Logger(os.path.splitext(sys.argv[1])[0])
    try:
    # Prints input
        logfile.write('=== Input file ===\n')
        with open(sys.argv[1]) as inp:
            for line in inp:
                logfile.write(line,end='')

        # Inits EMO parameters
        de_check   = 'de'   in params
        re_check   = 're'   in params
        beta_check = 'beta' in params

        if not de_check:
            params['de']   = 10000.
        if not re_check:
            params['re']   = 1.5
        if not beta_check:
            params['beta'] = [1.]

        init_guess_given = de_check and re_check and beta_check

        if not init_guess_given or params['refit']:
            d_e, r_e, beta = emo_init_fit(params['de'], params['re'], params['q'], params['rref'], params['beta'], r_inp, u_inp)
        else:
            d_e  = params['de']
            r_e  = params['re']
            beta = params['beta']

        logfile.write('\n=== Initial guess ===\n')

        # Calls solver
        levels = vr_solver(params, expdata, d_e, r_e, beta)

        logfile.write('Experimental energies + calculated with initial guess \n')

        # Prints energy levels
        for j, _ in levels.items():
            logfile.write(f'J = {j}\nv\tE_calc,cm-1\tE_exp,cm-1\tdiff')
            for v, lev in levels[j].items():
                logfile.write(f'{v}\t' +
                      '{:.3f}'.format(lev.energy) + '\t' +
                      '{:.3f}'.format(expdata[j][v]) + '\t' +
                      '{:.3f}'.format(lev.energy - expdata[j][v]))

        logfile.write('\nInitial PEC + EMO fit \n')

        # Prints ab intio PEC + initial EMO fit
        logfile.write('R,A\tU_init,cm-1\tU_EMO,cm-1\tdiff')
        for r, u in zip(r_inp, u_inp):
            u_fit = emo(r, d_e, r_e, params['rref'], params['q'], beta)
            logfile.write(f'{r}\t{u}\t{u_fit}\t{u - u_fit}')

        logfile.write('\n=== EMO parameters ===\n')

        # Prints EMO parameters
        logfile.write('De   '+'{:.3f}'.format(d_e))
        logfile.write('Re   '+'{:.3f}'.format(r_e))
        logfile.write('Beta ',end='')
        for n in range(len(beta)):
            logfile.write('{:.5e}  '.format(beta[n]),end='')

        logfile.write('\n\n=== Fitting procedure ===\n')

        # Calls fit routine
        with redirect_stdout(io.StringIO()) as f:
            d_e, r_e, beta = exp_fit(params, expdata, d_e, r_e, beta)
        logfile.write(f.getvalue())
        logfile.write('\n=== Results ===\n')


        # Calls solver
        levels = vr_solver(params, expdata, d_e, r_e, beta)

        logfile.write(' Experimental energies + calculated with fitted parameters \n')

        # Prints energy levels
        for j, _ in levels.items():
            logfile.write(f'J = {j}\nv\tE_calc,cm-1\tE_exp,cm-1\tdiff')
            for v, lev in levels[j].items():
                logfile.write(f'{v}\t' +
                      '{:.3f}'.format(lev.energy) + '\t' +
                      '{:.3f}'.format(expdata[j][v]) + '\t' +
                      '{:.3f}'.format(lev.energy - expdata[j][v]))

        logfile.write('\nInitial PEC + EMO fit \n')

        # Prints ab intio PEC + resulting EMO fit
        logfile.write('R,A\tU_init,cm-1\tU_EMO,cm-1\tdiff')
        for r, u in zip(r_inp, u_inp):
            u_fit = emo(r, d_e, r_e, params['rref'], params['q'], beta)
            logfile.write(f'{r}\t{u}\t{u_fit}\t{u - u_fit}')

        logfile.write('\n=== EMO parameters ===\n')

        # Prints EMO pars
        logfile.write('De   '+'{:.3f}'.format(d_e))
        logfile.write('Re   '+'{:.3f}'.format(r_e))
        logfile.write('Beta ')
        for n in range(len(beta)):
            logfile.write('{:.5e}  '.format(beta[n]),end='')

        logfile.write('\n\n EMO in range [Rmin, Rmax]')

        # Prints EMO in range [Rmin, Rmax]
        r_grid = linspace(params['rmin'], params['rmax'], 200)
        logfile.write('R,A\tU_EMO,cm-1')
        for r in r_grid:
            u_fit = emo(r, d_e, r_e, params['rref'], params['q'], beta)
            logfile.write(f'{r}\t{u_fit}')

        logfile.finalize()
    except KeyboardInterrupt:
        logfile.write("\n!!!THE SCRIPT WAS USER-INTERRUPTED!!!\n")
        logfile.finalize()

if __name__ == '__main__':

    # Physical constant values to use
    au_to_Da = 5.48579909065e-4
    au_to_cm = 219474.63067
    a0_to_A = 0.529177210903

    # Set grid point number for numerical solution of the radial Schrodinger equation
    ngrid = 50000

    main()
